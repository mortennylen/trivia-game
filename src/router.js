import Vue from 'vue';
import VueRouter from 'vue-router';
import StartScreen from './components/StartScreen.vue';
import QuestionsScreen from './components/Questions/QuestionsScreen.vue';
import ResultsScreen from './components/ResultsScreen.vue';

Vue.use(VueRouter); //Add the Router features to the Vue Object

const routes = [
    {
        path: '/start',
        alias: '/',
        component: StartScreen
    },
    {
        path: '/questions',
        component: QuestionsScreen
    },
    {
        path: '/results',
        component: ResultsScreen
    }

]

export default new VueRouter({ routes });