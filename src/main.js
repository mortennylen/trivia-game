import Vue from 'vue';
import App from './App.vue';
import store from './store';

// Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css';
// Router
import router from './router';


Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
