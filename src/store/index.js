import Vue from 'vue';
import Vuex from 'vuex';
import { TriviaAPI } from '@/api/TriviaAPI';


Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        categories: [],
        error: '',
        configurations: {},
        questions: [],
        questionNumber: 0,
        userAnswers: [],
        score: 0,

 
    },
    mutations: {
        setCategories: (state, payload) => {
            state.categories = payload;
        },

        setError: (state, payload) => {
            state.error = payload;
        },

        setConfigurations: (state, payload) => {
            state.configurations = payload;
        },

        setQuestions: (state, payload) => {
            state.questions = payload;
        },

        setQuestionNumber: (state, payload) => {
            state.questionNumber = payload;
        },

        setUserAnswers: (state, payload) => {
            state.userAnswers = payload;
        },

        setScore: (state, payload) => {
            state.score = payload;
        }

    },
    getters: {
        // Returns current question (the question with index of 'questionNumber')
        getCurrentQuestion: state => {
            return state.questions[state.questionNumber];
        },

        getCurrentAnswers: state => {
            // Combines correct answers and correct answer into one array
            const unshuffledAnswers = [...state.questions[state.questionNumber].incorrect_answers];
            unshuffledAnswers.push(state.questions[state.questionNumber].correct_answer);

            // Shuffles or randomly reorders array borrowed from https://javascript.info/task/shuffle
            function fisherYatesShuffle(array) {
                for (let i = array.length - 1; i > 0; i--) {
                  let j = Math.floor(Math.random() * (i + 1));
                  [array[i], array[j]] = [array[j], array[i]];
                }
                return array;
            }

            return fisherYatesShuffle(unshuffledAnswers) // returns array of answers shuffled 
        }

        

    },
    actions: {
        // Call API request asynchronously
        async fetchCategories ({ commit }) {
            try {
                const categories = await TriviaAPI.getCategories();
                commit('setCategories', categories)
            } catch (e) {
                commit ('setError', e.message)  
            }
        },

        // Call API request asynchronously
        async fetchQuestions({ commit, state }) {
            try {
                const questions = await TriviaAPI.getQuestions(state.configurations)
                commit('setQuestions', questions)
                commit('setScore', 0)
                commit('setQuestionNumber', 0)
                commit('setUserAnswers', [])
            } catch (e) {
                commit ('setError', e.message)
            }
        },
        // adds input (object) to userAnswers (array)
        addUserAnswers({commit, state}, input) {
            const userAnswer = (state.userAnswers).concat(input) // used .concat because push did not work 
            commit('setUserAnswers', userAnswer)
            
        }
        
    }

})
