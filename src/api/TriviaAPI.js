export const TriviaAPI = {
    getQuestions (configurations) {
        return fetch(`https://opentdb.com/api.php?amount=${configurations.numberOfQuestions}&category=${configurations.category}&difficulty=${configurations.difficulty}`)
        .then((response) => {
            return response.json();
        })
        .then((data => data.results));
    },
    
    getCategories() {
        return fetch("https://opentdb.com/api_category.php")
        .then((response) => response.json())
        .then((data) => data.trivia_categories);
    }
    
}
